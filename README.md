# Ideaworks Ansible Playbook

This [Ansible](http://www.ansible.com/home) playbook installs the [ideaworks](http://github.com/dstl/ideaworks)
web application on a CentOS Server. It has been tested on CentOS 6.x, but probably won't work on a CentOS 7.x. 

The ansible script can be used on its own to deploy to a CentOS server you have already have running or
it can be used in conjunction with [Vagrant](https://www.vagrantup.com), which will
 [provision](http://en.wikipedia.org/wiki/Provisioning#Server_provisioning) the machine for you.


# Getting Up & Running

* Download and install **VirtualBox** for your operating system (tested with 4.3.12)
* Download and install **Vagrant** for your operating system (tested with 1.6.5)
* Download and install **Ansible** for your operating system (tested with 1.7.2)

* Have a poke around with both Ansible and Vagrant or if you want to jump straight in

  1. Edit /webapp/vars/main.yml so that `application_name` is correct for your instance and so that 
  `host_name` matches this line: `config.vm.network "private_network", ip: "192.168.50.32"` in the Vagrantfile.

  2. 
	```
	$> cd istarter-ansible
	$> vagrant up
	
	```
  
  3. Vagrant will grab a box template and set it up in VirtualBox for you. It will then use Ansible to 
  build the instance of ideaworks for you.
  
  4. Hit the IP you provided in /webapp/vars/main.yml (and Vagrantfile): http://<ip-from-vagrantfile>/istarter 


# Stack

More information about the stack used in ideaworks is available on the [ideaworks](http://github.com/dstl/ideaworks).
Here are a couple of ansible-specific bits. This package is reasonably large because the dependencies (excluding those
available via yum) are provided (see any of the `files` directories in each role). If you wish to update any of the
dependencies (mongodb, mod_wsgi, etc.) replace the files and re-run.

## Python

This uses the python 2.6 that ships with CentOS 6.5 and installs dependencies directly into
the main site-packages instance using pip. I did originally look at using a virtualenv, but ran into difficulties
configuring it all automatically via ansible.

## Django Superuser

The ansible script uses the `django_manage` module to run `syncdb`. At the moment, it skips through
the command prompts, so you will need to create a superuser if you want to use the django admin site.

To do so, ssh onto the deployment box and type:

    $> cd /opt/ideaworks/backend/ideaworks
    $> python manage.py createsuperuser
    
You will now be able to login to the admin site using those credentials: /ideaworks_api/admin/

# Main Code Repo

The main istarter repo can be found at [ideaworks](http://github.com/dstl/ideaworks). The Ansible and Vagrant content
will eventually get rolled into the Dstl repo.